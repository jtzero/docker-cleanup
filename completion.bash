if [ -z "${BASH_SOURCE[0]:-}" ]; then
  # zsh
  DOCKER_CLEANUP_FILES_DIR="$( cd "$( dirname "${(%):-%N}" )" && pwd )"
else
  DOCKER_CLEANUP_FILES_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
fi

complete -W "$("${DOCKER_CLEANUP_FILES_DIR}/docker-cleanup" commands)" docker-cleanup
