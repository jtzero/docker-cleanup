# Docker Cleanup
## A script for cleaning up assets left behind by docker

the top level regex command removes a container and all it's linked items
example to cleanup all 'Exited' containers and it's associated images volumes networks:
`docker-cleanup regex 'Exited'`

remove all images without a tag or repository
`docker-cleanup none`

remove all volumes:
`docker-cleanup volumes all`

remove all images creaetd `7 days ago`:
`docker-cleanup images '7 days ago'`

```
docker-cleanup (by-)?regex ($options)? ${regex_matching_something_in_docker_ps} ($docker-cmd-options)?
docker-cleanup (all-containers|all-c)
docker-cleanup volumes (all|${regex_matching_something_in_docker_volume_ls})? ($docker-cmd-options)?
docker-cleanup volumes # with no args will prune
docker-cleanup none
docker-cleanup (by-)?image-name ${docker_image_name}
docker-cleanup images ${regex_matching_something_in_docker_ps} ($docker-cmd-options)?
docker-cleanup stop-containers ${regex_matching_something_in_docker_ps}
docker-cleanup (rm|containers) ($options)? ${regex_matching_something_in_docker_ps}
docker-cleanup secrets ${regex_matching_something_in_docker_secret_ls}
docker-cleanup version
options: -ir|--include-running
docker-cmd-options: options specific to that docker command
```

## Known oddities/TODO
- `docker-cleanup (by-)?image-name ${docker_image_name}` is kind of redundent as it's the same as `docker rmi some-image-name`
- `docker-cleanup none` should actually be something like `docker-cleanup images none` as it shoud belong under the images command
- `docker-cleanup (all-containers|all-c)` should be `docker-cleanup containers all` to match volumes 
- that being said the `all` under volumes should be `--all` or somthing of that nature designating it as a reserved word and not regex,
  all is basically a readable version of `'.*'`
