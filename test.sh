#!/usr/bin/env bash

if [ -z "${BASH_SOURCE[0]:-}" ]; then
  # zsh
  DOCKER_CLEANUP_DIR="${0:A:h}"
else
  DOCKER_CLEANUP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
fi

#PATH="${DOCKER_CLEANUP_DIR}/test:$PATH"

"${DOCKER_CLEANUP_DIR}/test/bats/bin/bats" "${DOCKER_CLEANUP_DIR}"/test/*.bats "${DOCKER_CLEANUP_DIR}"/test/**/*.bats
