#!./test/libs/bats/bin/bats

load './test_helper.bash'

@test 'secrets, with no arguments, will print usage' {
    run ${SCRIPT} secrets
    assert_failure 2
}
