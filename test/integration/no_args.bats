#!./test/libs/bats/bin/bats

load './test_helper.bash'

@test 'docker-cleanup, with no args, will print usage' {
    run ${SCRIPT}
    assert_failure 2
}
