#!./test/libs/bats/bin/bats

load '../test_helper.bash'

setup() {
  docker() {
    echo ""
  }
  export -f docker
}

teardown() {
  unset -f docker
}
