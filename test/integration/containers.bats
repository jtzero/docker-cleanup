#!./test/libs/bats/bin/bats

load './test_helper.bash'

@test 'containers, with no args, will print usage' {
    run ${SCRIPT} containers
    assert_failure 2
}

@test 'rm, an alias, with no args, will print usage' {
    run ${SCRIPT} rm
    assert_failure 2
}

@test 'containers when a regex does not match text from `docker ps -a`, containers will still succeed' {
    run ${SCRIPT} containers "DoesNoMatch"
    assert_success
}
