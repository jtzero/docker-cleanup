#!./test/libs/bats/bin/bats

load './test_helper.bash'

@test 'volumes, with no args, will prune by default' {
    run ${SCRIPT} volumes
    assert_success
}
