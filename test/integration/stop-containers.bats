#!./test/libs/bats/bin/bats

load './test_helper.bash'

@test 'stop-containers, with no args, will print usage' {
    run ${SCRIPT} stop-containers
    assert_failure 2
}

@test 'stop-containers, when no containers running, will still succeed' {
    run ${SCRIPT} stop-containers "DoesNotMatch"
    assert_success
}
