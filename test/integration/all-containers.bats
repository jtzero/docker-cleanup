#!./test/bats/bin/bats

load './test_helper.bash'

@test 'all-containers, will attempt to remove all containers' {
    run ${SCRIPT} all-containers
    assert_success
}
