#!./test/libs/bats/bin/bats

load './test_helper.bash'

@test 'regex, with no args, will print usage' {
    run ${SCRIPT} regex
    assert_failure 2
}

@test 'by-regex, an alias, with no args, will print usage' {
    run ${SCRIPT} by-regex
    assert_failure 2
}

@test 'regex, when no regex matches, will still succeed' {
    run ${SCRIPT} regex "DoesNotMatch"
    assert_success
}
