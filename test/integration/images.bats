#!./test/libs/bats/bin/bats

load './test_helper.bash'

@test 'images, with no args, will print usage' {
    run ${SCRIPT} images
    assert_failure 2
}

@test 'images, when regex does not match will still succeed' {
    run ${SCRIPT} images "DoesNotMatch"
    assert_success
}

@test 'image-name, with no args, will print usage' {
    run ${SCRIPT} image-name
    assert_failure 2
}

@test 'image-name, when name does not match, it will still succeed' {
    run ${SCRIPT} image-name "DoesNotMatch"
    assert_success
}
