if [ -z "${BASH_SOURCE}" ]; then
  # zsh
  DOCKER_CLEANUP_TEST_DIR="$( cd "$( dirname "${(%):-%N}" )" && pwd )"
else
  DOCKER_CLEANUP_TEST_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
fi

DOCKER_CLEANUP_DIR="$(dirname $DOCKER_CLEANUP_TEST_DIR)"

load "${DOCKER_CLEANUP_TEST_DIR}/test_helper/bats-support/load.bash"
load "${DOCKER_CLEANUP_TEST_DIR}/test_helper/bats-assert/load.bash"

SCRIPT="${DOCKER_CLEANUP_DIR}/docker-cleanup"
