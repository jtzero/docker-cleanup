#!./test/libs/bats/bin/bats

load '../test_helper.bash'

setup() {
  docker() {
    case $1 in
      inspect) echo "AN_IMAGE_ID\nANOTHER_IMAGE_ID"
      ;;
    esac
  }
  export -f docker
  export TESTING=1
}

teardown() {
  unset -f docker
}
