#!./test/bats/bin/bats

load './test_helper.bash'

@test 'get_image_ids_from_container_ids, will get a list of image ids from thier corresponding container ids' {
    load "${SCRIPT}"
    result="$(get_image_ids_from_container_ids "1234" "4567")"
    set +u # needed for bats
    [ "${result}" = "AN_IMAGE_ID\nANOTHER_IMAGE_ID" ]
}
