#!./test/bats/bin/bats

load './test_helper.bash'

@test "Should add numbers together" {
    assert_equal $(echo 1+1 | bc) 2
}
